#!/usr/bin/env node

var program = require('commander');
var fs = require('fs');
var Q = require('Q');
var path = require('path');
var fsys = require('file-system');

var fs_mkdir = Q.denodeify(fs.mkdir);
var fs_writeFile = Q.denodeify(fs.writeFile);
var fs_readdir = Q.denodeify(fs.readdir);

var templatedir = path.normalize(__dirname+'/../template');

var appName;
program
  .version('0.0.1')
  .usage('<applicationName> | -t | -h | -V')
  .option('-t, --templatedir', 'output template directory')
  .arguments('<applicationName>')
  .action(function (name) {
     appName = name;
  })
  .on('--help', function(){
  })
  .parse(process.argv);

if (!appName) {
  if(process.argv[2] == '-t' || process.argv[2] == '--templatedir') {
    console.log(templatedir)
  }else{
    program.outputHelp();
  }
  process.exit(0);
}

Q.when()
  .then(function(){
    return fs_readdir.call(fs, templatedir)
      .catch(function(ex){

      });
  }).then(function(files){
    if(files&&files.length>0){
      // ToDo: copy template
      fsys.copySync(
        templatedir,
        './'+appName,
        {
          noProcess: ['package.json'],
          process: function(contents, filepath) {
          }
        }
      );
    }else if(!files || files.length==0){
      return fs_mkdir.call(fs, './'+appName)
        .then(function(err, written, buffer){
          var mainjs =
            "'use strict';\n"+
            "\n"+
            "var app = require('app');\n"+
            "var BrowserWindow = require('browser-window');\n"+
            "\n"+
            "require('crash-reporter').start();\n"+
            "\n"+
            "var mainWindow = null;\n"+
            "\n"+
            "app.on('window-all-closed', function() {\n"+
            "  if (process.platform != 'darwin') {\n"+
            "    app.quit();\n"+
            "  }\n"+
            "});\n"+
            "\n"+
            "app.on('ready', function() {\n"+
            "  mainWindow = new BrowserWindow({width: 800, height: 600});\n"+
            "  mainWindow.loadURL('file://' + __dirname + '/renderer/html/index.html');\n"+
            "\n"+
            "  mainWindow.on('closed', function() {\n"+
            "    mainWindow = null;\n"+
            "  });\n"+
            "});\n";
          return fs_writeFile.call(fs, './'+appName+'/main.js', mainjs)
      }).then(function(err, written, buffer){
        return fs_mkdir.call(fs, './'+appName+'/renderer')
      }).then(function(err){
        return fs_mkdir.call(fs, './'+appName+'/renderer/html')
      }).then(function(err){
        var indexhtml =
          "<!DOCTYPE html>"+
          "<html>\n"+
          "  <head>\n"+
          "    <meta charset='UTF-8'>\n"+
          "    <title>"+appName+"</title>\n"+
          "  </head>\n"+
          "  <body>\n"+
          "    <p>Hello "+appName+"</p>\n"+
          "  </body>\n"+
          "</html>";
        return fs_writeFile.call(fs, './'+appName+'/renderer/html/index.html', indexhtml);
      }).then(function(err, written, buffer){
        return fs_mkdir.call(fs, './'+appName+'/renderer/js')
      }).then(function(err){
        return fs_mkdir.call(fs, './'+appName+'/renderer/css')
      });
    }else{
      process.exit(0);
    }
  }).finally(function(){
    var package = {
      "name": appName,
      "version": "0.0.1",
      "description": "",
      "main": "main.js",
      "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
      },
      "keywords": [],
      "author": "",
      "license": "ISC",
      "dependencies": {
      }
    };
    fs.writeFile('./'+appName+'/package.json', JSON.stringify(package, null, '  '),function(err){
    });
  }).catch(function(ex){
    console.log(ex.message)
  })
