# Electron Template Generator
This is a template generator for Electron Application.

## Installation

```bash
$ npm -g install electron-template
```

## Usage

```bash
$ electron-template <applicationName>
```

The above command will create the following files/directories.

```
<appicationName>
    ├ main.js
    ├ package.json
    └ renderer
        ├ html
        │   └ index.html
        ├ js
        └ css
```

#### Using Original template
NOTICE: a following function works on v0.0.2 or later.

If you make your own template files in a 'template' directory,
 the above command will just copy the files in the 'template' directory to new electron application directory.

The 'template' directory is shown by the following command.

```bash
$ electron-template -t
```
You can open the 'template' directory by the following command.

Mac OS X:
```bash
$ open `electron-template -t`
```

Windows (in Command Prompt):
```bash
Z:¥> for /f %A in ('electron-template -t') do explorer %A
```
